
-- all artists with "d" in name
SELECT * FROM artists WHERE name LIKE "%d%";


-- all songs with length of less than 350
SELECT * FROM songs WHERE length < 350;


-- joining albums and songs tables (only album name, song name, song length)
SELECT albums.album_title, songs.song_name, songs.length FROM albums 
	JOIN songs ON albums.id = songs.album_id;


-- joining artists and albums tables + only with letter "a" on name
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE name LIKE "%a%";


-- sorting albums in Z-A order (only first 4 records)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;


-- joining albums and songs tables + albums sorted in Z-A order
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC;